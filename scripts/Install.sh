#
# 2017-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#

mkdir -p /usr/local/tomcat/webapps
cd /usr/local/tomcat/webapps
rm -rf *

wget "https://github.com/opensourceBIM/BIMserver/releases/download/v$BIMSERVER_VERSION/bimserverwar-$BIMSERVER_VERSION.war" -O "ROOT.war"
